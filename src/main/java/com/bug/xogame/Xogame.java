/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bug.xogame;

import java.util.Scanner;
/**
 *
 * @author OS
 */
public class Xogame {
    public static void main(String[] args){
        Scanner kb = new Scanner(System.in);
        int row, col;
        char win = '-';
        int turn = 0;
        int count = 0;
        System.out.println("Welcome to the XO Game");
         String[][] box = new String[3][3];
        box[0][0] = "-";
        box[0][1] = "-";
        box[0][2] = "-";
        box[1][0] = "-";
        box[1][1] = "-";
        box[1][2] = "-";
        box[2][0] = "-";
        box[2][1] = "-";
        box[2][2] = "-";
        System.out.println("  1 2 3");
        for (int i = 0; i < box.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < box.length; j++) {
                System.out.print(box[i][j] + " ");
            }
            System.out.println();
        }
        do {
            System.out.println("X Turn");
            System.out.println("Please input Row  Col : ");
            if (count % 2 == 0) {
                row = kb.nextInt();
                col = kb.nextInt();
                box[row - 1][col - 1] = "X";
                System.out.println("  1 2 3");
                for (int i = 0; i < box.length; i++) {
                    System.out.print(i + 1 + " ");
                    for (int l = 0; l < box.length; l++) {
                        System.out.print(box[i][l] + " ");
                    }
                    System.out.println();
                }
                count++;
            }
            if (box[0][0].equals("X") && box[0][1].equals("X") && box[0][2].equals("X")) {
                win = 'x';
                System.out.println("Player X is the WINNER !!");
                break;
            }
            if (box[1][0].equals("X") && box[1][1].equals("X") && box[1][2].equals("X")) {
                win = 'x';
                System.out.println("Player X is the WINNER !!");
                break;
            }
            if (box[2][0].equals("X") && box[2][1].equals("X") && box[2][2].equals("X")) {
                win = 'x';
                System.out.println("Player X is the WINNER !!");
                break;
            }
            if (box[0][0].equals("X") && box[0][1].equals("X") && box[0][2].equals("X")) {
                win = 'x';
                System.out.println("Player X is the WINNER !!");
                break;
            }
            if (box[1][0].equals("X") && box[1][1].equals("X") && box[1][2].equals("X")) {
                win = 'x';
                System.out.println("Player X is the WINNER !!");
                break;
            }
            if (box[2][0].equals("X") && box[2][1].equals("X") && box[2][2].equals("X")) {
                win = 'x';
                System.out.println("Player X is the WINNER !!");
                break;
            }
            if (box[0][0].equals("X") && box[1][1].equals("X") && box[2][2].equals("X")) {
                win = 'x';
                System.out.println("Player X is the WINNER !!");
                break;
            }
            if (box[0][2].equals("X") && box[1][1].equals("X") && box[2][0].equals("X")) {
                win = 'x';
                System.out.println("Player X is the WINNER !!");
                break;
            }
            turn++;
            if (count % 2 != 0) {
                System.out.println("O Turn");
                System.out.println("Please input Row  Col : ");
                row = kb.nextInt();
                col = kb.nextInt();
                box[row - 1][col - 1] = "O";
                System.out.println("  1 2 3");
                for (int i = 0; i < box.length; i++) {
                    System.out.print(i + 1 + " ");
                    for (int j = 0; j < box.length; j++) {
                        System.out.print(box[i][j] + " ");
                    }
                    System.out.println();
                }
                count++;
                if (box[0][0].equals("O") && box[0][1].equals("O") && box[0][2].equals("O")) {
                    win = 'o';
                    System.out.println("Player O is the WINNER !!");
                    break;
                }
                if (box[1][0].equals("O") && box[1][1].equals("O") && box[1][2].equals("O")) {
                    win = 'o';
                    System.out.println("Player O is the WINNER !!");
                    break;
                }
                if (box[2][0].equals("O") && box[2][1].equals("O") && box[2][2].equals("O")) {
                    win = 'o';
                    System.out.println("Player O is the WINNER !!");
                    break;
                }
                if (box[0][0].equals("O") && box[0][1].equals("O") && box[0][2].equals("O")) {
                    win = 'o';
                    System.out.println("Player O is the WINNER !!");
                    break;
                }
                if (box[1][0].equals("O") && box[1][1].equals("O") && box[1][2].equals("O")) {
                    win = 'o';
                    System.out.println("Player O is the WINNER !!");
                    break;
                }
                if (box[2][0].equals("O") && box[2][1].equals("O") && box[2][2].equals("O")) {
                    win = 'o';
                    System.out.println("Player O is the WINNER !!");
                    break;
                }
                if (box[0][0].equals("O") && box[1][1].equals("O") && box[2][2].equals("O")) {
                    win = 'o';
                    System.out.println("Player O is the WINNER !!");
                    break;
                }
                if (box[0][2].equals("O") && box[1][1].equals("O") && box[2][0].equals("O")) {
                    win = 'o';
                    System.out.println("Player O is the WINNER !!");
                    break;
                }
                turn++;
            }
        } while (turn < 9);
        if (win == '-') {
            System.out.println("Player Draw");
        } else {
            System.out.println("");
        }
        System.out.println("Bye Bye");
    }
}

